<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<c:set var="i" value="0"/>
<script>
var qtdPagina = 10;
var pagAtual = 0;
var jsonAutores = JSON.parse('${autores}');
var tamanho = Object.keys(jsonAutores).length;
function paginar(){	
	$('table > tbody > tr').remove();
	var tbody = $('table > tbody');		
	for (var i = pagAtual * qtdPagina; i < tamanho  && i < (pagAtual + 1) * qtdPagina; i++){
		var autor = jsonAutores[i];
		tbody.append(				
				$("<tr id='autor_"+autor.id+"'>")
				.append($("<td>").append(autor.id))
				.append($('<td>').append(autor.nome))
				.append($('<td>').append("<a href='alterar_autor?id="+autor.id+"'><span class='glyphicon glyphicon-pencil'></span></a>"))
				.append($('<td>').append("<a href='excluir_autor?id="+autor.id+"'><span class='glyphicon glyphicon-remove'></span></a>"))
		);
	}
	$('#numeracao').text('P�gina '+(pagAtual + 1) + ' de '+Math.ceil(tamanho/qtdPagina));
}
function configBotoes(){
	$('#proximo').prop('disabled',tamanho <= qtdPagina || pagAtual > tamanho/qtdPagina - 1);
	$('#anterior').prop('disabled', tamanho <= qtdPagina || pagAtual == 0);
}
$(function(){
	$('#proximo').click(function(){
		if(pagAtual < tamanho / qtdPagina -1){
			pagAtual++;
			paginar();
			configBotoes();
		}
	});
	$('#anterior').click(function(){
		if(pagAtual > 0){
			pagAtual--;
			paginar();
			configBotoes();
		}
	});
	paginar();
	configBotoes();
});
</script>


<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nome</th>
					<th>Alterar</th>
					<th>Excluir</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		<div id="comand_pag">
			<button class="btn btn-default" id="anterior" disabled>&lsaquo;
				Anterior</button>
			<span id="numeracao"></span>
			<button class="btn btn-default" id="proximo" disabled>Pr�ximo
				&rsaquo;</button>
		</div>
	</div>
	<div class="col-sm-2"></div>
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />