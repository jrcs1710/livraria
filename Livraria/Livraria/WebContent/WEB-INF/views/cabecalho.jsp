
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Cool Bookstore</title>
<link
	href="<c:url value="/resources/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>"
	rel="stylesheet">
<link href=<c:url value="/resources/css/estilo.css"/> rel="stylesheet">
<script src="<c:url value="/resources/jquery/jquery-1.11.3.min.js"/>"></script>
<script
	src="<c:url value="/resources/bootstrap-3.3.6-dist/js/bootstrap.min.js"/>"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="container">
		<header class="page_header">
			<img src="<c:url value='/resources/imagens/logo.png' />" />
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#myMenu">
							<span class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="myMenu">
						<ul class="nav navbar-nav">
							<li><a href="<c:url value='/index'/>">Home</a></li>		
							<li><a href="<c:url value='/livros'/>">Livros</a></li>		
							
							<c:if
								test="${usuarioLogado != null && usuarioLogado.tipo.ordinal() == 0}">
								<li class="dropdown"><a class="dropdown-toggle"
									data-toggle="dropdown" href="#">Usu�rio <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<c:url value='/admin/form_usuario'/>">Novo
												usu�rio</a></li>
										<li><a href="<c:url value='/admin/lista_usuario'/>">Lista
												de usu�rios</a></li>
									</ul></li>
								<li class="dropdown"><a class="dropdown-toggle"
									data-toggle="dropdown" href="#">Autor <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<c:url value='/admin/form_autor'/>">Novo
												autor</a></li>
										<li><a href="<c:url value='/admin/lista_autor'/>">Lista
												de autores</a></li>
									</ul></li>
								<li class="dropdown"><a class="dropdown-toggle"
									data-toggle="dropdown" href="#">G�nero<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<c:url value='/admin/form_genero'/>">Novo
												g�nero</a></li>
										<li><a href="<c:url value='/admin/lista_genero'/>">Lista
												de g�neros</a></li>
									</ul></li>
								<li class="dropdown"><a class="dropdown-toggle"
									data-toggle="dropdown" href="#">Editora<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<c:url value='/admin/form_editora'/>">Nova
												editora</a></li>
										<li><a href="<c:url value='/admin/lista_editora'/>">Lista
												de editoras</a></li>
									</ul></li>
								<li class="dropdown"><a class="dropdown-toggle"
									data-toggle="dropdown" href="#">Livro<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="<c:url value='/admin/form_livro'/>">Novo
												livro</a></li>
										<li><a href="<c:url value='/admin/lista_livro'/>">Lista
												de livros</a></li>
									</ul></li>
									<li><a href="<c:url value='/admin/logout'/>">Logout</a></li>	
							</c:if>
						</ul>
					</div>
				</div>
			</nav>
		</header>