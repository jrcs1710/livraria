<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<div class="row">
	<div class="col-sm-4"></div>
	<section class="form col-sm-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Cadastro de Editora</h3>
			</div>
			<div class="panel-body">
				<form action="salvar_editora" method="post">
					<div class="form-group">
						<label for="input_id">Id:</label> <input type="text"
							class="form-control input-lg" name="id" id="input_id"
							readonly="readonly" value="${editora.id }">
					</div>
					<div class="form-group">
						<label for="input_nome">Nome:</label> <input type="text"
							class="form-control input-lg" id="input_nome" name="nome"
							placeholder="Informe o nome" required="required"
							value="${editora.nome }">
					</div>
					<button type="submit" class="btn btn-default input-lg">Salvar</button>
				</form>
			</div>
		</div>
	</section>
	<div class="col-sm-4"></div>
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />