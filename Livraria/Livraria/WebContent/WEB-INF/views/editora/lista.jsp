<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<c:set var="i" value="0" />
<script>
function excluir(id){
	$.post("excluir_editora",{'id':id},function(){
		$('#editoraR_'+id).hide('slow');
	});
}	
</script>
<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Descri��o</th>
					<th>Alterar</th>
					<th>Excluir</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${editoras }" var="editora">
					<tr id="editoraR_${editora.id }">
						<td>${editora.id }</td>
						<td>${editora.nome}</td>
						<td><a href="alterar_editora?id=${editora.id}"><span class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="#" onclick="excluir(${editora.id})"><span class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
	<div class="col-sm-2"></div>
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />