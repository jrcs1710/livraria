<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<div class="row">
	<div class="col-sm-4"></div>
	<div class="col-sm-4" style="text-align: center;">
		<h1><span class="glyphicon glyphicon-ok"></span></h1>
		<h2>Editora ${editora.nome } salva com sucesso</h2>
	</div>
	<div class="col-sm-4"></div>
</div>
<script lang="JavaScript">
		setTimeout("document.location = 'form_editora'", 1500);
</script>
<c:import url="/WEB-INF/views/rodape.jsp" />