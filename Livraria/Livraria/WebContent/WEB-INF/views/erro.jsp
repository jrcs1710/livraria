<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />

	<h1>OPS, OCORREU UM ERRO</h1>
	<h5>Erro: ${pageContext.exception}</h5>
	<h5>URI: ${pageContext.errorData.requestURI}</h5>
	<h5>C�digo: ${pageContext.errorData.statusCode}</h5>
	<h5>Stack trace:</h5>
	<c:forEach var="trace" items="${pageContext.exception.stackTrace}">
		<h5>${trace}</h5>
	</c:forEach>
	<br /> <br />

<c:import url="/WEB-INF/views/rodape.jsp" />
