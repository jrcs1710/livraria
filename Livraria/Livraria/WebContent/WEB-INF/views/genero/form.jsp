<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<div class="row">
	
	<section class="form col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Cadastro de G�nero</h3>
			</div>
			<div class="panel-body">
				<form action="salvar_genero" method="get" accept-charset="UTF-8">
					<div class="form-group">
						<label for="input_id">Id:</label> <input type="text"
							class="form-control input-lg" name="id" id="input_id"
							readonly="readonly" value="${genero.id }">
					</div>
					<div class="form-group">
						<label for="input_descricao">Descri��o:</label> <input type="text"
							class="form-control input-lg" id="input_descricao" name="descricao"
							placeholder="Informe a descri��o" required="required" value="${genero.descricao }">
							<form:errors path="genero.descricao" cssStyle="color:red"  />
					</div>
					<button type="submit" class="btn btn-default input-lg">Salvar</button>
				</form>
			</div>
		</div>
	</section>
	
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />