<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<c:set var="i" value="0" />
<script>
function excluir(id){
	$.post("excluir_genero",{'id':id},function(){
		$('#generoR_'+id).hide('slow');
	});
}	
</script>
<div class="row">
	
	<div class="col-sm-8 col-sm-offset-2">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Descri��o</th>
					<th>Alterar</th>
					<th>Excluir</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${generos }" var="genero">
					<tr id="generoR_${genero.id }">
						<td>${genero.id }</td>
						<td>${genero.descricao }</td>
						<td><a href="alterar_genero?id=${genero.id}"><span class="glyphicon glyphicon-pencil"></span></a></td>
						<td><a href="#" onclick="excluir(${genero.id})"><span class="glyphicon glyphicon-remove"></span></a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
	
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />