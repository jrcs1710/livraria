<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<div class="row">
	<div class="col-sm-2"></div>
	<section class="form col-sm-8 text-center">
		<h1>Bem-vindo � Cool BookStore<br/> ${usuarioLogado.nome }</h1>
	</section>
	<div class="col-sm-2"></div>
</div>
<div class="row" style="text-align: center;">	
		<img src="<c:url value='/resources/imagens/livraria.jpg' />" class="img-rounded" />
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />