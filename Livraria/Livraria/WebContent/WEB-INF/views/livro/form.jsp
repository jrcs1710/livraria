<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<link href=<c:url value="/resources/fileinput/css/fileinput.css"/>
	media="all" rel="stylesheet" type="text/css" />
<script src=<c:url value="/resources/fileinput/js/fileinput.min.js"/>
	type="text/javascript"></script>
<div class="row">
	<div class="col-sm-3"></div>
	<section class="form col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Cadastro de Livro</h3>
			</div>
			<div class="panel-body">
				<form action="salvar_livro" method="post"
					enctype="multipart/form-data">
					<div class="form-group">
						<label for="input_id">Id:</label> <input type="text"
							class="form-control input-lg" name="id" id="input_id"
							readonly="readonly" value="${livro.id }">
					</div>
					<div class="form-group">
						<label for="input_titulo">T�tulo:</label> <input type="text"
							class="form-control input-lg" id="input_titulo" name="titulo"
							placeholder="Informe o t�tulo" required="required"
							value="${livro.titulo }">
					</div>
					<div class="form-group">
						<label for="input_edicao">Edi��o:</label> <input type="number"
							class="form-control input-lg" id="input_edicao" name="edicao"
							placeholder="Informe a edi��o" required="required"
							value="${livro.edicao}">
					</div>
					<div class="form-group">
						<label for="input_paginas">N�mero de p�ginas:</label> <input
							type="number" maxlength="10" class="form-control input-lg"
							id="input_paginas" name="numPaginas"
							placeholder="Informe o n�mero de p�ginas" required="required"
							value="${livro.numPaginas}">
					</div>
					<div class="form-group">
						<label for="input_data">Data de lan�amento:</label> <input
							type="date"  class="form-control input-lg"
							id="input_data" name="data"
							 required="required"
							value="<fmt:formatDate pattern='yyyy-MM-dd' value='${livro.data.time}'/>">
					</div>
					<div class="form-group">
						<label for="input_isbn">ISBN:</label> <input type="text"
							maxlength="10" class="form-control input-lg" id="input_isbn"
							name="ISBN" placeholder="Informe o ISBN" required="required"
							value="${livro.ISBN }">
					</div>
					<div class="form-group">
						<label for="select_genero">G�nero:</label> <select name="genero.id"
							id="select_genero" class="form-control input-lg">
							<c:forEach items="${generos }" var="g">
								<option value="${g.id }"
									<c:if test="${livro.genero.id.equals(g.id) }">selected</c:if>>${g.descricao }</option>
							</c:forEach>
						</select>
					</div>

					<div class="form-group">
						<label for="select_autor">Autor:</label> <select name="autor.id"
							id="select_autor" class="form-control input-lg">
							<c:forEach items="${autores }" var="a">
								<option value="${a.id }"
									<c:if test="${livro.autor.id.equals(a.id) }">selected</c:if>>${a.nome }</option>
							</c:forEach>
						</select>
					</div>

					<div class="form-group">
						<label for="select_editora">Editora:</label> <select name="editora.id"
							id="select_editora" class="form-control input-lg">
							<c:forEach items="${editoras }" var="e">
								<option value="${e.id}"
									<c:if test="${livro.editora.id.equals(e.id) }">selected</c:if>>${e.nome }</option>
							</c:forEach>
						</select>
					</div>


					<c:choose>
						<c:when test="${livro != null  && livro.capa != null}">
							<div style="text-align: center;">
								<img class="imgForm" src="<c:url value='..${livro.capa}'/>">
								<input type="hidden" name="foto" value="${livro.capa }">
							</div>
						</c:when>
						<c:otherwise>
							<div class="form-group">
								<label for="input_capa">Capa:</label><input type="file"
									class="file file-loading" data-show-upload="false"
									data-show-caption="false" id="input_capa" name="fileCapa"
									data-allowed-file-extensions='["jpg", "png"]'>
							</div>
						</c:otherwise>
					</c:choose>
					<button type="submit" class="btn btn-default input-lg">Salvar</button>
				</form>
			</div>
		</div>
	</section>
	<div class="col-sm-3"></div>
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />