<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tag"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<link href=<c:url value="/resources/fileinput/css/fileinput.css"/>
	media="all" rel="stylesheet" type="text/css" />
<script src=<c:url value="/resources/fileinput/js/fileinput.min.js"/>
	type="text/javascript"></script>
<div class="row">

	<section class="form col-sm-8 col-sm-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Informa��es do Livro</h3>
			</div>

			<div class="panel-body">
				<div class="col-sm-7">
					<tag:campoEspecial titulo="Id" valor="${livro.id }" />
					<tag:campoEspecial titulo="T�tulo" valor="${livro.titulo }" />
					<tag:campoEspecial titulo="Edi��o" valor="${livro.edicao }" />
					<tag:campoEspecial titulo="P�ginas" valor="${livro.numPaginas }" />
					<p>
						<strong>Lan�amento: </strong>
						<fmt:formatDate value='${livro.data.time }' pattern='dd/MM/yyyy' />
					</p>
					<tag:campoEspecial titulo="ISBN" valor="${livro.ISBN }" />
					<tag:campoEspecial titulo="Autor" valor="${livro.autor.nome }" />
					<tag:campoEspecial titulo="G�nero"
						valor="${livro.genero.descricao}" />

					<button onclick="window.history.back()"
						class="btn btn-default input-lg">Voltar</button>
				</div>
				<div class="col-sm-4">
					<img width="250px" src="<c:url value="${livro.capa }"/>">
				</div>
			</div>
		</div>
	</section>
</div>

<div class="row">
	<section class="col-sm-8 col-sm-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Avalia��es do Livro</h3>
			</div>
			<div class="panel-body">
				<c:forEach items="${classificacoes }" var="classificacao">
					<div class="row">
						<div class="col-sm-3">
							<img width="128px"
								src="<c:url value="${classificacao.usuario.foto }"/>"> <br />
							<c:forEach var="i" begin="0" end="4">
								<c:if test="${i < classificacao.avaliacao }">
									<img width="20px"
										src="<c:url value="/resources/imagens/star_full.png"/>">
								</c:if>
								<c:if test="${i >= classificacao.avaliacao }">
									<img width="20px"
										src="<c:url value="/resources/imagens/star_empty.png"/>">
								</c:if>
							</c:forEach>
						</div>
						<div class="col-sm-9">${classificacao.comentario }</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</section>
</div>


<c:import url="/WEB-INF/views/rodape.jsp" />