<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<c:set var="i" value="0" />
<script>
var qtdPagina = 10;
var pagAtual = 0;
var jsonLivros = JSON.parse('${livros}');
var tamanho = Object.keys(jsonLivros).length;
function paginar(){	
	$('table > tbody > tr').remove();
	var tbody = $('table > tbody');		
	for (var i = pagAtual * qtdPagina; i < tamanho  && i < (pagAtual + 1) * qtdPagina; i++){
		var livro = jsonLivros[i];
		console.log(livro);
		tbody.append(				
				$("<tr>")				
				.append($('<td>').append("<img class='imgTable' src='."+livro.capa+"'/>"))				
				.append($('<td>').append(livro.titulo))
				.append($('<td>').append(livro.autor.nome))
				.append($('<td>').append("<a href='infoLivro?id="+livro.id+"'><span class='glyphicon glyphicon-info-sign'></span></a>"))
		);
	}
	$('#numeracao').text('P�gina '+(pagAtual + 1) + ' de '+Math.ceil(tamanho/qtdPagina));
}
function configBotoes(){
	$('#proximo').prop('disabled',tamanho <= qtdPagina || pagAtual > tamanho/qtdPagina - 1);
	$('#anterior').prop('disabled', tamanho <= qtdPagina || pagAtual == 0);
}
$(function(){
	$('#proximo').click(function(){
		if(pagAtual < tamanho / qtdPagina -1){
			pagAtual++;
			paginar();
			configBotoes();
		}
	});
	$('#anterior').click(function(){
		if(pagAtual > 0){
			pagAtual--;
			paginar();
			configBotoes();
		}
	});
	paginar();
	configBotoes();
});
</script>


<div class="row">
	<div class="col-sm-1"></div>
	<div class="col-sm-2">
		<h5>G�neros</h5>
		<ul>
			<c:forEach items="${generos }" var="g">
				<li><a href="buscarLivros?idGenero=${g.id }">${g.descricao }</a>
			</c:forEach>
		</ul>
	</div>

	<div class="col-sm-8">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Capa</th>
					<th>T�tulo</th>
					<th>Autor</th>
					<th>+ Info</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		<div id="comand_pag">
			<button class="btn btn-default" id="anterior" disabled>&lsaquo;
				Anterior</button>
			<span id="numeracao"></span>
			<button class="btn btn-default" id="proximo" disabled>Pr�ximo
				&rsaquo;</button>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />