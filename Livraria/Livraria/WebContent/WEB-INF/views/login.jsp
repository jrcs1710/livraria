<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<div class="row">
	<div class="col-sm-4"></div>
	<section class="form col-sm-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Bem-vindo � Cool BookStore</h3>
			</div>
			<div class="panel-body">
				<h4 style="color:red">${msg }</h4>
				<form action="logar" method="post">
					<div class="form-group">
						<label for="input_login">Login:</label> <input type="text"
							name="login" class="form-control input-lg" id="input_login"
							placeholder="Informe o login" required="required">
					</div>
					<div class="form-group">
						<label for="input_senha">Senha:</label> <input type="password"
							name="senha" class="form-control input-lg" id="input_senha"
							placeholder="Informe a senha" required="required">
					</div>
					<button type="submit" class="btn btn-default input-lg">Entrar</button>
				</form>
			</div>
		</div>
	</section>
	<div class="col-sm-4"></div>
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />