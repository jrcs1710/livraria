<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/WEB-INF/views/cabecalho.jsp" />
<link href=<c:url value="/resources/fileinput/css/fileinput.css"/>
	media="all" rel="stylesheet" type="text/css" />
<script src=<c:url value="/resources/fileinput/js/fileinput.min.js"/>
	type="text/javascript"></script>
<div class="row">
	<section class="form col-sm-6 col-sm-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Cadastro de Usu�rio</h3>
			</div>
			<div class="panel-body">
				<form action="salvar_usuario" method="post"
					enctype="multipart/form-data">
					<div class="form-group">
						<label for="input_id">Id:</label> <input type="text"
							class="form-control input-lg" name="id" id="input_id"
							readonly="readonly" value="${usuario.id }">
					</div>
					<div class="form-group">
						<label for="input_nome">Nome:</label> <input type="text"
							class="form-control input-lg" id="input_nome" name="nome"
							placeholder="Informe o nome" required="required"
							value="${usuario.nome }">
					</div>
					<div class="form-group">
						<label for="input_login">Login:</label> <input type="text"
							class="form-control input-lg" id="input_login" name="login"
							placeholder="Informe o login" required="required"
							value="${usuario.login}">
					</div>
					<div class="form-group">
						<label for="input_senha">Senha:</label> <input type="password"
							maxlength="10" class="form-control input-lg" id="input_senha"
							name="senha" placeholder="Informe a senha" required="required"
							value="${usuario.senha}">
					</div>
					<div class="form-group">
						<label for="select_tipo">Tipo:</label> 
						<select name="tipo"
							id="select_tipo" class="form-control input-lg">
							<c:forEach items="${tipoUsuario }" var="t">
								<option value="${t }"
									<c:if test="${usuario.tipo.equals(t) }">selected</c:if>>${t.toString() }</option>
							</c:forEach>
						</select>
					</div>
					<c:choose>
						<c:when test="${usuario != null }">
							<div style="text-align: center;">
								<img class="imgForm" src="<c:url value='..${usuario.foto}'/>">
								<input type="hidden" name="foto" value="${usuario.foto }">
							</div>
						</c:when>
						<c:otherwise>
							<div class="form-group">
								<label for="input_foto">Foto:</label><input type="file"
									class="file file-loading" data-show-upload="false"
									data-show-caption="false" id="input_foto" name="fileFoto"
									data-allowed-file-extensions='["jpg", "png"]'>
							</div>
						</c:otherwise>
					</c:choose>
					<button type="submit" class="btn btn-default input-lg">Salvar</button>
				</form>
			</div>
		</div>
	</section>	
</div>
<c:import url="/WEB-INF/views/rodape.jsp" />