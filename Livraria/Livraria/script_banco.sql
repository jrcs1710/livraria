create database if not exists livraria;
use livraria;
create table if not exists autor(
	id int auto_increment primary key,
	nome varchar(255) unique not null
);