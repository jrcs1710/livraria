package br.senai.sp.livraria.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;

import br.senai.sp.livraria.dao.AutorDao;
import br.senai.sp.livraria.modelo.Autor;

@Controller
public class AutorController {
	@Autowired
	private AutorDao dao;
	
	@RequestMapping("admin/form_autor")
	public String form() {
		return "autor/form";
	}
	
	@RequestMapping("admin/lista_autor")
	public String lista(Model model){
		List<Autor> lista = dao.listar();
		Gson json = new Gson();
		
		model.addAttribute("autores", json.toJson(lista));
		return "autor/lista";
	}
	
	@RequestMapping("admin/alterar_autor")
	public String alterar(long id, Model model){
		Autor a = dao.buscar(id);
		if (a != null) {
			model.addAttribute("autor",a);
		}
		return "forward:form_autor";
	}
		
	@RequestMapping("admin/salvar_autor")
	public String salvar(Autor autor){
		if (autor.getId() == null) {
			dao.inserir(autor);
		}else{
			dao.atualizar(autor);
			return "redirect:lista_autor";
		}
		return "autor/sucesso";
	}
	
	@RequestMapping("admin/excluir_autor")
	public String excluir(long id){
		dao.excluir(id);
		return "redirect:lista_autor";
	}

	
}
