package br.senai.sp.livraria.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;

import br.senai.sp.livraria.dao.InterfaceDao;
import br.senai.sp.livraria.modelo.Autor;
import br.senai.sp.livraria.modelo.Editora;
import br.senai.sp.livraria.modelo.Genero;

@Transactional
@Controller
public class EditoraController {
	@Autowired
	@Qualifier("jpaEditoraDao")
	private InterfaceDao<Editora> dao;
	
	@RequestMapping("admin/form_editora")
	public String form(){
		return "editora/form";
	}
	
	@RequestMapping("admin/salvar_editora")
	public String salvar(Editora editora){		
		if (editora.getId() == null) {
			dao.inserir(editora);
		}else{
			dao.alterar(editora);
		}
		return "editora/sucesso";
	}
	
	@RequestMapping("admin/lista_editora")
	public String lista(Model model){			
		model.addAttribute("editoras", dao.listar());
		return "editora/lista";
	}
	
	@RequestMapping("admin/alterar_editora")
	public String alterar(long id, Model model){
		Editora e = dao.buscar(id);
		if (e != null) {
			model.addAttribute("editora",e);
		}
		return "forward:form_editora";
	}
	
	@RequestMapping("admin/excluir_editora")
	public void excluir(long id, HttpServletResponse response){
		dao.excluir(id);
		response.setStatus(200);
	}
}
