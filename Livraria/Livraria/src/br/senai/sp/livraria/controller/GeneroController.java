package br.senai.sp.livraria.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import br.senai.sp.livraria.dao.InterfaceDao;
import br.senai.sp.livraria.modelo.Genero;

@Controller
@Transactional
public class GeneroController {
	@Autowired
	@Qualifier("jpaGeneroDao")
	private InterfaceDao<Genero> dao;
	
	@RequestMapping("admin/form_genero")
	public String form(){
		return "genero/form";
	}
	
	@RequestMapping("/admin/salvar_genero")
	public String salvar(@Valid Genero genero, BindingResult result){
		if (result.hasFieldErrors("descricao")) {
			return "genero/form";
		}
		if (genero.getId() == null) {
			dao.inserir(genero);
		}else{
			dao.alterar(genero);
			return "redirect:lista_genero";
		}
		return "genero/sucesso";
	}
	
	@RequestMapping("admin/lista_genero")
	public String lista(Model model){
		model.addAttribute("generos",dao.listar());
		return "genero/lista";
	}
	
	@RequestMapping("admin/alterar_genero")
	public String alterar(long id, Model model){
		Genero g = dao.buscar(id);
		if (g != null) {
			model.addAttribute("genero",g);
		}
		return "forward:form_genero";
	}
	
	@RequestMapping("admin/excluir_genero")
	public void excluir(long id, HttpServletResponse response){
		dao.excluir(id);
		response.setStatus(200);
	}
}
