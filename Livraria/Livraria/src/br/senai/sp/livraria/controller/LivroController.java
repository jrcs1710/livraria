package br.senai.sp.livraria.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import br.senai.sp.livraria.dao.AutorDao;
import br.senai.sp.livraria.dao.InterfaceDao;
import br.senai.sp.livraria.dao.JpaClassificacaoDao;
import br.senai.sp.livraria.dao.JpaLivroDao;
import br.senai.sp.livraria.modelo.Classificacao;
import br.senai.sp.livraria.modelo.Editora;
import br.senai.sp.livraria.modelo.Genero;
import br.senai.sp.livraria.modelo.Livro;
import br.senai.sp.livraria.modelo.Usuario;

@Controller
@Transactional
public class LivroController implements ServletContextAware {
	private ServletContext servletContext;
	@Autowired
	@Qualifier("jpaGeneroDao")
	private InterfaceDao<Genero> generoDao;
	@Autowired
	@Qualifier("jpaEditoraDao")
	private InterfaceDao<Editora> editoraDao;
	@Autowired
	private AutorDao autorDao;
	@Autowired
	private JpaClassificacaoDao classifiDao;
	@Autowired
	@Qualifier("jpaLivroDao")
	private InterfaceDao<Livro> livroDao;

	@RequestMapping("admin/form_livro")
	public String form(Model model) {
		model.addAttribute("autores", autorDao.listar());
		model.addAttribute("editoras", editoraDao.listar());
		model.addAttribute("generos", generoDao.listar());
		return "livro/form";
	}

	@RequestMapping("admin/salvar_livro")
	public String salvar(Livro livro, MultipartFile fileCapa) {
		String path = null;
		if (fileCapa != null && !fileCapa.isEmpty()) {
			String arquivo = fileCapa.getOriginalFilename();
			String extensao = arquivo.substring(arquivo.length() - 4);
			path = "/uploads/fotos_capas/" + livro.getTitulo().replace(" ", "").replace(".", "")
					+ System.currentTimeMillis() + extensao;
			livro.setCapa(path);
		}
		try {
			if (livro.getId() == null) {
				livroDao.inserir(livro);
			} else {
				livroDao.alterar(livro);
			}
			if (livro.getCapa() != null && fileCapa != null && !fileCapa.isEmpty()) {
				byte[] bytes = fileCapa.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(servletContext.getRealPath(path))));
				stream.write(bytes);
				stream.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return "livro/sucesso";
	}

	@RequestMapping("admin/lista_livro")
	public String lista(Model model) {
		List<Livro> lista = livroDao.listar();
		Gson json = new Gson();
		model.addAttribute("livros", json.toJson(lista));
		return "livro/lista";
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@RequestMapping("admin/excluir_livro")
	public String excluir(long id) {
		Livro l = livroDao.buscar(id);
		livroDao.excluir(id);
		if (l.getCapa() != null) {
			File f = new File(servletContext.getRealPath(l.getCapa()));
			f.delete();
		}
		return "redirect:lista_livro";
	}
	
	@RequestMapping("admin/alterar_livro")
	public String alterar(long id, Model model) {
		Livro l = livroDao.buscar(id);
		if (l != null) {
			model.addAttribute("livro",l);
		}
		return "forward:form_livro";	
	}
	
	@RequestMapping("livros")
	public String livros(Model model, HttpServletRequest request){
		if (request.getAttribute("livros")== null) {
			model.addAttribute("livros", new Gson().toJson(livroDao.listar()));
		}
		model.addAttribute("generos", generoDao.listar());
		return "livro/livros";
	}
	
	@RequestMapping("buscarLivros")
	public String buscarLivros(long idGenero, Model model){
		List<Livro> livros = ((JpaLivroDao) livroDao).buscarPorGenero(idGenero);
		Gson json = new Gson();
		model.addAttribute("livros", json.toJson(livros));
		return "forward:livros";
	}
	
	@RequestMapping("infoLivro")
	public String infoLivro(long id, Model model){
		Livro livro = livroDao.buscar(id);
		model.addAttribute("livro",livro);
		List<Classificacao> classificacoes = classifiDao.listar(id);
		model.addAttribute("classificacoes", classificacoes);
		return "livro/info";
	}
}
