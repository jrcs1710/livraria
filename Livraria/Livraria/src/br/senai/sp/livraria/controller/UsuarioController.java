package br.senai.sp.livraria.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import br.senai.sp.livraria.dao.InterfaceDao;
import br.senai.sp.livraria.dao.JpaUsuarioDao;
import br.senai.sp.livraria.modelo.Autor;
import br.senai.sp.livraria.modelo.TipoUsuario;
import br.senai.sp.livraria.modelo.Usuario;

@Transactional
@Controller
public class UsuarioController implements ServletContextAware {
	private ServletContext servletContext;
	@Autowired
	@Qualifier("jpaUsuarioDao")
	private InterfaceDao<Usuario> dao;

	@RequestMapping("admin/form_usuario")
	public String form(Model model) {
		model.addAttribute("tipoUsuario", TipoUsuario.values());
		return "usuario/form";
	}

	@RequestMapping("admin/salvar_usuario")
	public String salvar(Usuario usuario, MultipartFile fileFoto) {
		String path = null;
		if (fileFoto != null && !fileFoto.isEmpty()) {
			String arquivo = fileFoto.getOriginalFilename();
			String extensao = arquivo.substring(arquivo.length() - 4);
			path = "/uploads/fotos_usuarios/" + usuario.getLogin() + extensao;
			usuario.setFoto(path);
		}
		try {
			if (usuario.getId() == null) {
				dao.inserir(usuario);
			} else {
				dao.alterar(usuario);
			}
			if (usuario.getFoto() != null && fileFoto != null && !fileFoto.isEmpty()) {
				byte[] bytes = fileFoto.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(servletContext.getRealPath(path))));
				stream.write(bytes);
				stream.close();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return "usuario/sucesso";
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@RequestMapping("admin/lista_usuario")
	public String lista(Model model) {
		List<Usuario> lista = dao.listar();
		Gson json = new Gson();
		model.addAttribute("usuarios", json.toJson(lista));
		return "usuario/lista";
	}

	@RequestMapping("admin/alterar_usuario")
	public String alterar(long id, Model model) {
		Usuario u = dao.buscar(id);
		if (u != null) {
			model.addAttribute("usuario",u);
		}
		return "forward:form_usuario";	
	}
	
	@RequestMapping("admin/excluir_usuario")
	public String excluir(long id){
		Usuario user = dao.buscar(id);
		dao.excluir(id);
		if (!user.getFoto().isEmpty()) {
			File f = new File(servletContext.getRealPath(user.getFoto()));
			f.delete();
		}
		return "redirect:lista_usuario";
	}
	
	@RequestMapping("admin/logar")
	public String logar(String login, String senha,HttpSession session, Model model){
		JpaUsuarioDao usuarioDao = (JpaUsuarioDao) dao;
		Usuario user = usuarioDao.logar(login, senha);
		if (user != null) {
			session.setAttribute("usuarioLogado", user);
			return "index";	
		}else{
			model.addAttribute("msg", "Usuário e/ou senha inválidos");
			return "login";	
		}
		
	}
	
	@RequestMapping("admin/logout")
	public String logout(HttpSession session){
		session.invalidate();
		return "login";
	}
}
