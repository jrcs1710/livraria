package br.senai.sp.livraria.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.senai.sp.livraria.modelo.Autor;

@Repository
public class AutorDao {
	private Connection conexao;
	
	@Autowired
	public AutorDao(DataSource dataSource){
		try {
			this.conexao = dataSource.getConnection();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void inserir(Autor autor){
		String sql = "INSERT INTO autor(nome) VALUES(?)";
		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setString(1, autor.getNome());
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Autor> listar(){
		String sql = "SELECT * FROM autor";
		List<Autor> lista = new ArrayList<>();
		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {				
				Autor a = new Autor();
				a.setId(rs.getLong("id"));
				a.setNome(rs.getString("nome"));
				lista.add(a);
			}
			stmt.close();
			rs.close();			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return lista;
	}
	
	public Autor buscar(long id){
		String sql = "SELECT * FROM autor WHERE id = ?";
		Autor autor = null;
		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				autor = new Autor();
				autor.setId(rs.getLong("id"));
				autor.setNome(rs.getString("nome"));
			}
			rs.close();
			stmt.close();			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return autor;
	}
	
	public void atualizar(Autor autor){
		String sql = "UPDATE autor SET nome = ? WHERE id = ?";
		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setString(1, autor.getNome());
			stmt.setLong(2, autor.getId());
			stmt.execute();
			stmt.close();			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void excluir(long id){
		String sql = "DELETE FROM autor WHERE id = ?";
		try {
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setLong(1, id);
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
