package br.senai.sp.livraria.dao;

import java.util.List;

import org.springframework.stereotype.Repository;


public interface InterfaceDao<T> {
	public T buscar(long id);
	public List<T> listar();
	public void inserir(T objeto);
	public void alterar(T objeto);
	public void excluir(long id);
}
