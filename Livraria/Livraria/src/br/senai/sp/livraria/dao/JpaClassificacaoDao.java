package br.senai.sp.livraria.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.senai.sp.livraria.modelo.Classificacao;

@Repository
public class JpaClassificacaoDao {
	@PersistenceContext
	private EntityManager manager;

	public void inserir(Classificacao classificacao) {
		manager.persist(classificacao);
	}

	public List<Classificacao> listar(Long idLivro) {
		TypedQuery<Classificacao> query = manager
				.createQuery("select c from Classificacao c where c.livro.id = :idLivro", Classificacao.class);
		query.setParameter("idLivro", idLivro);
		return query.getResultList();
	}
}
