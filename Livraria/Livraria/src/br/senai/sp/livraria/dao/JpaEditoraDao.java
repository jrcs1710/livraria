package br.senai.sp.livraria.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.senai.sp.livraria.modelo.Editora;

@Repository
public class JpaEditoraDao implements InterfaceDao<Editora> {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Editora buscar(long id) {
		return manager.find(Editora.class, id);
	}

	@Override
	public List<Editora> listar() {
		TypedQuery<Editora> query = manager.createQuery("select e from Editora e", Editora.class);
		return query.getResultList();
	}

	@Override
	public void inserir(Editora editora) {
		manager.persist(editora);		
	}

	@Override
	public void alterar(Editora editora) {
		manager.merge(editora);		
	}

	@Override
	public void excluir(long id) {
		Editora e = buscar(id);
		manager.remove(e);		
	}

}
