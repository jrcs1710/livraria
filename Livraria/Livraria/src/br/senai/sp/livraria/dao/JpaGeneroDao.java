package br.senai.sp.livraria.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.senai.sp.livraria.modelo.Genero;

@Repository
public class JpaGeneroDao implements InterfaceDao<Genero>{
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Genero buscar(long id) {
		return manager.find(Genero.class, id);
	}

	@Override
	public List<Genero> listar() {
		TypedQuery<Genero> query = manager.createQuery("select g from Genero g order by g.descricao",Genero.class);
		return query.getResultList();
	}

	@Override
	public void inserir(Genero genero) {
		manager.persist(genero);
		
	}

	@Override
	public void alterar(Genero genero) {
		manager.merge(genero);		
	}

	@Override
	public void excluir(long id) {
		Genero genero = buscar(id);
		manager.remove(genero);		
	}
 
}
