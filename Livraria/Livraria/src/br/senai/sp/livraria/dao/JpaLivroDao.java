package br.senai.sp.livraria.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.senai.sp.livraria.modelo.Classificacao;
import br.senai.sp.livraria.modelo.Livro;

@Repository
public class JpaLivroDao implements InterfaceDao<Livro> {
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Livro buscar(long id) {
		return manager.find(Livro.class, id);
	}

	@Override
	public List<Livro> listar() {
		TypedQuery<Livro> query = manager.createQuery("select l from Livro l", Livro.class);
		return query.getResultList();
	}

	@Override
	public void inserir(Livro livro) {
		manager.persist(livro);
	}

	@Override
	public void alterar(Livro livro) {
		manager.merge(livro);
	}

	@Override
	public void excluir(long id) {
		Livro l = buscar(id);
		manager.remove(l);
	}

	public List<Livro> buscarPorGenero(long idGenero) {
		TypedQuery<Livro> query = manager.createQuery("select l from Livro l where l.genero.id = :id", Livro.class);
		query.setParameter("id", idGenero);
		return query.getResultList();
	}

	public List<Livro> buscarPorUsuario(long idUsuario) {
		TypedQuery<Livro> query = manager.createQuery("select c.livro from Classificacao c where c.usuario.id = :id",
				Livro.class);
		query.setParameter("id", idUsuario);
		return query.getResultList();
	}
}
