package br.senai.sp.livraria.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.senai.sp.livraria.modelo.TipoUsuario;
import br.senai.sp.livraria.modelo.Usuario;

@Repository
public class JpaUsuarioDao implements InterfaceDao<Usuario> {
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Usuario buscar(long id) {
		return manager.find(Usuario.class, id);
	}

	@Override
	public List<Usuario> listar() {
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u order by u.nome", Usuario.class);
		return query.getResultList();
	}

	@Override
	public void inserir(Usuario usuario) {
		manager.persist(usuario);
	}

	@Override
	public void alterar(Usuario usuario) {
		manager.merge(usuario);
	}

	@Override
	public void excluir(long id) {
		Usuario u = buscar(id);
		manager.remove(u);

	}
	
	public Usuario logar(String login, String senha){
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha and u.tipo = :tipo", Usuario.class);
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		query.setParameter("tipo", TipoUsuario.ADMINISTRADOR);
		if (query.getResultList().size() != 0) {
			return query.getResultList().get(0);
		}else{
			return null;
		}		
	}
	
	public Usuario logarCliente(String login, String senha){
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u where u.login = :login and u.senha = :senha and u.tipo = :tipo", Usuario.class);
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		query.setParameter("tipo", TipoUsuario.CLIENTE);
		if (query.getResultList().size() != 0) {
			return query.getResultList().get(0);
		}else{
			return null;
		}		
	}

}
