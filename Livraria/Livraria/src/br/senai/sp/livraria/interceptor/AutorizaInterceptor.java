package br.senai.sp.livraria.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class AutorizaInterceptor extends HandlerInterceptorAdapter{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String uri = request.getRequestURI();
		if(uri.endsWith("loginAdm") || uri.endsWith("logar")){
			return true;
		}else if (uri.contains("admin") && request.getSession().getAttribute("usuarioLogado") == null ) {
			response.sendRedirect("	loginAdm");
			return false;
		}
		return true;
	}
}
