package br.senai.sp.livraria.modelo;

public enum TipoUsuario {
	ADMINISTRADOR("Administrador"), CLIENTE("Cliente");

	private String tipo;

	private TipoUsuario(String tipo) {
		this.tipo = tipo;
	}

	public String toString() {
		return tipo;
	};
}
