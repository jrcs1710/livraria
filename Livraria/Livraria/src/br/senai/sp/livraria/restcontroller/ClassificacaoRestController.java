package br.senai.sp.livraria.restcontroller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.livraria.dao.JpaClassificacaoDao;
import br.senai.sp.livraria.modelo.Classificacao;
import br.senai.sp.livraria.modelo.Livro;
import br.senai.sp.livraria.modelo.Usuario;

@Transactional
@RestController
@RequestMapping("/services/classificacao")
public class ClassificacaoRestController {
	@Autowired
	private JpaClassificacaoDao dao;
	
	@RequestMapping(value = "/nova", method = RequestMethod.POST, headers = "Accept=application/json;charset=UTF-8")
	public String nova(@RequestBody String json){		
		System.out.println(json);
		try {
			JSONObject jsonOb = new JSONObject(json);
			Classificacao classificacao = new Classificacao();
			classificacao.setAvaliacao(jsonOb.getDouble("avaliacao"));
			classificacao.setComentario(jsonOb.getString("comentarios"));
			Livro livro = new Livro();
			livro.setId(jsonOb.getLong("idLivro"));
			classificacao.setLivro(livro);
			Usuario usuario = new Usuario();
			usuario.setId(jsonOb.getLong("idUsuario"));
			classificacao.setUsuario(usuario);
			dao.inserir(classificacao);
			return "OK";
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
	
}
