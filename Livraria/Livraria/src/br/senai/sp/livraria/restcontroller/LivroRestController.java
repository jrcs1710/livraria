package br.senai.sp.livraria.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.senai.sp.livraria.dao.InterfaceDao;
import br.senai.sp.livraria.dao.JpaLivroDao;
import br.senai.sp.livraria.dao.JpaUsuarioDao;
import br.senai.sp.livraria.modelo.Classificacao;
import br.senai.sp.livraria.modelo.Livro;

@RestController
@RequestMapping("/services/livros")
public class LivroRestController {
	@Autowired
	@Qualifier("jpaLivroDao")
	private InterfaceDao<Livro> dao;

	@RequestMapping(value = "/lista", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public List<Livro> lista(){
		return dao.listar();
	}
	
	@RequestMapping(value = "/meus_livros/{idUsuario}", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public List<Livro> listaMeusLivros(@PathVariable(value="idUsuario") long idUsuario){
		System.out.println(idUsuario);
		return ((JpaLivroDao)dao).buscarPorUsuario(idUsuario);
	}
}
