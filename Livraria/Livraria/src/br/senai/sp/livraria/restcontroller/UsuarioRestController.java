package br.senai.sp.livraria.restcontroller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.ServletContext;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;

import com.google.gson.Gson;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import br.senai.sp.livraria.dao.InterfaceDao;
import br.senai.sp.livraria.dao.JpaUsuarioDao;
import br.senai.sp.livraria.modelo.TipoUsuario;
import br.senai.sp.livraria.modelo.Usuario;

@Transactional
@RestController
@RequestMapping("/services/usuario")
public class UsuarioRestController implements ServletContextAware {
	private ServletContext servletContext;
	@Autowired
	@Qualifier("jpaUsuarioDao")
	private InterfaceDao<Usuario> dao;

	@RequestMapping(value = "/lista", method = RequestMethod.GET, headers = "Accept=application/json;charset=UTF-8")
	public List<Usuario> lista() {
		return dao.listar();
	}

	@RequestMapping(value = "/salvar", method = RequestMethod.POST, headers = "Accept=application/json;charset=UTF-8")
	public String salvar(@RequestBody String json) {
		String retorno;
		System.out.println(json);
		try {
			// converte a String json no objeto Usuario
			JSONObject jsonOb = new JSONObject(json);
			Usuario usuario = new Usuario();
			if (!jsonOb.isNull("id")) {
				usuario.setId(jsonOb.getLong("id"));
			}
			usuario.setLogin(jsonOb.getString("login"));
			usuario.setSenha(jsonOb.getString("senha"));
			usuario.setNome(jsonOb.getString("nome"));
			usuario.setTipo(TipoUsuario.values()[jsonOb.getInt("tipo")]);
			if (!jsonOb.isNull("foto")) {
				byte[] foto = Base64.decode(jsonOb.getString("foto"));
				String path = "/uploads/fotos_usuarios/" + usuario.getLogin() + ".jpg";
				usuario.setFoto(path);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(servletContext.getRealPath(path))));
				stream.write(foto);
				stream.close();
			}
			if (usuario.getId() == null) {
				dao.inserir(usuario);
				retorno = "Usuário " + usuario.getNome() + " inserido com sucesso";
			} else {
				dao.alterar(usuario);
				retorno = "Dados atualizados com sucesso";
			}
			return retorno;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value = "/logar", method = RequestMethod.POST, headers = "Accept=application/json;charset=UTF-8")
	public Usuario logar(@RequestBody String json) {
		System.out.println(json);
		Usuario usuario = null;
		try {
			JSONObject job = new JSONObject(json);
			String login = job.getString("login");
			String senha = job.getString("senha");
			usuario = ((JpaUsuarioDao) dao).logarCliente(login, senha);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return usuario;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;

	}
}
